/**
 * @author Mücahid Dayan
 * Clock Picker Angular Module
 */

let getTemplateFile = (name, ext = "html") => {
  return name + "." + ext;
};
let timePicker = angular.module("timepicker", []);

const FOURFIFTHS = 0.8;
const THREEFIFTHS = 0.6;

timePicker.controller("timepickerController", [() => {}]);

let spion;

timePicker.directive("timepicker", [
  "timepickerDateService",
  "$document",
  "$window",
  "$timeout",
  (timepickerDateService, $document, $window, $timeout) => {
    let link = (scope, element, attributes, controllers) => {
      const date = {
        setHours: hours => {
          date.hours = hours > 9 ? hours : "0" + hours;
        },
        setMinutes: minutes => {
          date.minutes = minutes > 9 ? minutes : "0" + minutes;
        },
        getHours: () => {
          return date.hours;
        },
        getMinutes: () => {
          return date.minutes;
        },
        getDuration: (stringify = false) => {
          return stringify
            ? JSON.stringify({
                hour: date.getHours(),
                minute: date.getMinutes()
              })
            : `${date.hours}:${date.minutes}`;
        }
      };

      let convertToNumber = val => {
        return val >> 0;
      };

      let isAttributeSet = attribute => {
        return attribute in attributes;
      };

      scope.tpRequired =
        angular.isDefined("scope.tpMin") || angular.isDefined("scope.tpBind")
          ? "required"
          : "";
      try {
        scope.tpDisplayed = timepickerDateService.timeToDateObject(
          scope.tpModel
        );
      } catch (e) {
        scope.tpDisplayed = timepickerDateService.timeToDateObject("00:00");
      }

      scope.time = {
        hour: 0,
        minute: 15,
        validateHour: () => {
          let hour = convertToNumber(scope.time.hour);
          hour %= 24;
          if (hour < 0) hour += 24;
          scope.time.hour = hour;
        },
        validateMinute: () => {
          let minute = convertToNumber(scope.time.minute);
          minute %= 60;
          if (minute < 0) minute += 60;
          scope.time.minute = minute;
        },
        update: () => {
          date.setHours(scope.time.hour);
          date.setMinutes(scope.time.minute);
          // scope.tpModel = date.getDuration();
          var UTCDate = new Date();
          UTCDate.setMinutes(scope.time.minute);
          UTCDate.setHours(scope.time.hour);
          UTCDate.setMilliseconds(0);
          UTCDate.setSeconds(0);
          scope.tpDisplayed = UTCDate;
          scope.tpModel = date.getDuration();
        },
        value: null
      };

      if (angular.isDefined(attributes.tpBind)) {
        scope.$watch("tpBind", () => updateBindings());
      }

      let updateBindings = () => {
        if (angular.isDefined(attributes.tpBind)) {
          try {
            scope.tpMin = timepickerDateService.toTimeString(
              timepickerDateService.addToTime(scope.tpBind, scope.tpDiff)
            );
          } catch (e) {
            scope.tpMin = timepickerDateService.getTimeStringFromDate("00:00");
          }
        }
      };

      let init = () => {
        try {
          let time = timepickerDateService.convertToTimeObject(scope.tpModel);
          scope.time.hour = time.hour;
          scope.time.minute = time.minute;
          scope.time.update();          
        } catch (e) {
          scope.time.hour = 0;
          scope.time.minute = 0;
        }
      };

      init();

      if(!scope.tpModel){
        $timeout(()=>init(),100);
      }

      scope.clock = {
        active: false,
        activate: element => {
          scope.clock.deactivateAll();
          angular.element(element).addClass("active");
        },
        deactivateAll: () =>
          [].map.call(document.querySelectorAll(".timepicker"), cp =>
            angular.element(cp).removeClass("active")
          ),
        toggle: () => {
          scope.clock.opened = !scope.clock.opened;
        },
        open: () => {
          scope.clock.opened = true;
          scope.tpModel = null;
        },
        close: () => {
          scope.clock.opened = false;
          scope.time.update();
        },
        done: () => {
          scope.time.update();
          scope.clock.close();
          $timeout(() => scope.tpDone(), 100);
        },

        opened: false,
        disabled: isAttributeSet("disabled")
      };

      scope.prevent = () => {
        scope.clock.open();        
        angular
          .element(element)
          .find("input")[2]
          .focus();
          return false;
      };

      if (!angular.isDefined(scope.tpDiff)) {
        scope.tpDiff = 0;
      }

      updateBindings();

      $document.on("keypress, keydown", event => {
        if (event.which == 27) {
          // escape
          scope.$apply(scope.clock.close());
        }
        if (event.which == 13) {
          scope.$apply(scope.clock.done());
        }
      });

      $document.on("click", event => {
        if (angular.element(event.target).hasClass("timepicker")) {
          scope.clock.activate(event.target);
        } else {
          scope.clock.deactivateAll();
        }
      });

      let destroy = () => {
        $document.off("keypress");
        $document.off("click");
      };

      scope.$on("$destroy", () => {
        destroy();
      });
    };

    let template = (element, attribute) =>
      `
     <div class="timepicker-wrapper">
      <input id="timepicker-input" ng-required="{{tpRequired}}" type="time" ng-model="tpDisplayed" ng-click="clock.open()" ng-change="prevent()" ng-show="true" ng-attr-min="{{tpMin}}">
      <input id="output" type="text" ng-model="tpModel" ng-attr-name="{{name}}" ng-show="false" />
      <div class="timepicker" ng-form name="timepicker" ng-attr-placeholder="tpPlaceholder" ng-show="clock.opened" ng-attr-data-hour="time.hour"
          ng-attr-data-minute="time.minute">
      <div class="hour-wrapper tp-item">
          <input autofocus type="number" name="hour" id="hour" ng-model-options="{ debounce: 0 }" oninput="this.value=this.value>>0" ng-change="time.validateHour()" ng-model="time.hour"
          />
      </div>
      <span class="time-splitter">:</span>
      <div class="minute-wrapper tp-item">
          <input type="number" name="minute" id="minute" ng-model-options="{ debounce: 0 }" oninput="this.value=this.value>>0" ng-change="time.validateMinute()" ng-model="time.minute"
          />
      </div>
      <i class="button" ng-click="clock.done()">Done</i>
  </div>
  </div>
  `;

    return {
      restrict: "E",
      scope: {
        tpModel: "=?",
        tpPlaceholder: "@?",
        tpStringfyResult: "@?",
        tpMin: "@?",
        name: "@?",
        tpDone: "&?",
        tpBind: "=?",
        tpDiff: "@?"
      },
      template,
      link
    };
  }
]);

timePicker.service("timepickerDateService", function() {
  let isValidDate = value => {
    return !(new Date(value).toString() === "Invalid Date");
  };

  let isDate = value => value instanceof Date;

  let validateTimeString = value => {
    if (!/:/.test(value)) {
      throw new Error('Invalid Format for Timestring "HH:mm"');
    }
  };

  let getTimeStringFromDate = date => {
    if (!date) throw new Error("Date is not defined!");
    date = date.toString();
    validateTimeString(date);
    let start = date.indexOf(":") - 2;
    return date.substring(start, start + 8);
  };

  let parseTime = time => {
    if (!time) throw new Error("Time is not defined!");
    validateTimeString(time);
    time = time.split(":");
    return {
      hour: time[0] >> 0,
      minute: time[1] >> 0
    };
  };

  let isValidTimeObject = timeObject => {
    var result =
      typeof timeObject === "object" &&
      timeObject.hasOwnProperty("hour") &&
      timeObject.hasOwnProperty("minute");
    return result;
  };

  let convertToTimeObject = value => {
    if (isValidTimeObject(value)) {
      return value;
    }
    if (isDate(value) || isValidDate(value)) {
      value = getTimeStringFromDate(value);
    }
    validateTimeString(value);
    return parseTime(value);
  };

  let timeToDateObject = timeString => {
    if (!timeString) throw new Error("Time String is not defined");
    let date = new Date();
    try {
      var time = convertToTimeObject(timeString);
      date.setHours(time.hour);
      date.setMinutes(time.minute);

      date.setSeconds(0);
      date.setMilliseconds(0);
    } catch (e) {
      throw new Error("timeToDateObject:", e);
    }
    return date;
  };

  let secondToTimeObject = seconds => {
    if (!seconds && seconds !== 0) throw new Error("Seconds is not defined!");
    let minuteS = 60;
    hourS = minuteS * 60;

    let hour = ~~(seconds / hourS),
      l = seconds - hour * hourS;
    let minute = ~~(l / minuteS);
    l -= minute * minuteS;
    let second = ~~(l / 1000);

    return {
      hour,
      minute,
      second
    };
  };

  let addToTime = (time, seconds) => {
    if (!time && time !== 0) throw new Error('Parameter "time" is not defined');
    if (!seconds && seconds !== 0)
      throw new Error('Parameter "seconds" is not defined');
    try {
      seconds = secondToTimeObject(seconds);
      time = convertToTimeObject(time);
      time.hour += seconds.hour;
      time.minute += seconds.minute;
    } catch (e) {
      console.warn(e);
    }
    return time;
  };

  let toTimeString = time => {
    if (!isValidTimeObject(time))
      throw new Error("Parameter time is not a valid time object!");
    time.hour = time.hour < 10 ? "0" + time.hour : time.hour;
    time.minute = time.minute < 10 ? "0" + time.minute : time.minute;
    return `${time.hour}:${time.minute}`;
  };

  return {
    isValidDate,
    validateTimeString,
    getTimeStringFromDate,
    parseTime,
    convertToTimeObject,
    timeToDateObject,
    addToTime,
    toTimeString
  };
});
